# Modèles mathématiques appliquées à l'épidémiologie

https://collaboratif.cirad.fr/share/page/repository#filter=path%7C%2FSites%2FModelisationDynamique%2FdocumentLibrary

https://trello.com/b/tVuYj0PX/formation-mod%C3%A8les-dynamiques-17-21-24-26-juin-2024

Shrestha, Sourya, and James O. Lloyd-Smith, eds. “Introduction to Mathematical Modeling of Infectious Diseases,” DIMACS Series in Discrete Mathematics and Theoretical Computer Science, 75, no. 1 (October 8, 2010). https://doi.org/10.1090/dimacs/075.

Financement : **agreenium** Soutien à la création de formations doctorales
